function [fVarNames, gridSkip] = delSDFVars(fVarNames, dimension)
  % Function to remove higher-dimension variable names and types from 
  % fVarNames. 
  % Arguments:
  %   -> fVarNames: (n x 2) cell array of variable names, types
  %   -> dimension: EPOCH simulation dimension as integer (1,2)
  % Returns:
  %   -> fVarNames: updated cell array, without higher-dimensions

  % Check dimension
  if (dimension == 1)
    unusedVars = {'gy','gz','y','z'};  % Unused variables in 1D
    gridSkip = 2;
  elseif ( dimension == 2 )
    unusedVars = {'gz','z'};           % Unused variables in 2D
    gridSkip = 1;
  elseif ( dimension == 3 )
    fprintf('\nWarning: attempting to remove variables from 3D simulation.\n');
    unusedVars = {};
  else
    fprintf('\nError: dimension must be one of: (1, 2, 3). Aborting...\n')
    quit;
  end

  % Remove unused dimensions from fVarNames
  for dv = 1:length(unusedVars)
    deldv = find(strcmp(fVarNames, unusedVars{dv})); % find position
    fVarNames(deldv,:) = [];  % delete variable and type
  end
end
