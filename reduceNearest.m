function fVarReduced = reduceNearest(fVar, fVarReduced, cellIDs, ... 
  smethod, avMethod, superCell, varNames, oldDims, newDims, reduceDim, ...
  nDims)
  % Function to calculate the nearest neighbour for discarded particles,
  % and combine the particle weighting. Nearest neighbour searches are 
  % performed on groups of cells ("super-cells"), and the particles kept
  % are sampled evenly from the distribution of total momentum |P^2| within
  % each super-cell.
  % Arguments: 
  %   -> fVar         : struct containing the required simulation data
  %   -> fVarReduced  : struct of reduced variables
  %   -> cellIDs      : (1 x n) cell array of particle {x,y,z} references
  %   -> avMethod     : (1 x n) cell string array of attributes to average 
  %                     when combining particles using NN method
  %                       - (empty) : retain attributes of sampled 
  %                                   particles; only modify particle 
  %                                   weighting function
  %                       - 'mom'   : average particle momenta 
  %                       - 'pos'   : average particle positions
  %   -> superCell    : (1 x n) array of cell grouping divisions (how many
  %                     supercells to form in each dimension)
  %   -> varNames     : (k x 2) cell array of variable names, types
  %   -> oldDims      : (1 x n) array of old simulation dimensions 
  %   -> newDims      : (1 x n) array of new simulation dimensions
  %   -> reduceDim    : (1 x n) array of dimensions to reduce (0/1 = T/F)
  %   -> nDims        : number of simulation dimensions n
  % Returns:
  %   -> fVarReduced  : struct of reduced data 
  
  % Temporary particle attributes
  fVar_temp = struct;
  fVar_temp.('w') = fVar.('w');

  % If averaging attributes, create copies of data
  if ( ~isempty(avMethod) )
    skipAverage = 0;
    tempVarTypes = avMethod
    tempVarID = ismember(varNames, tempVarTypes); % (k x 2) logical array
    tempVarNames = varNames( find(tempVarID(:,2)), 1 );  
    for i = 1:length(tempVarNames)
      % copy attribute data
      fVar_temp.(tempVarNames{i}) = fVar.(tempVarNames{i});
    end
  else
    skipAverage = 1;
  end

  part_keep = []; % IDs of new pseudoparticles

  % Extract cell IDs %
  [cx, cy, cz] = cellIDs{:};  % (cy,) cz empty in (1D/)2D

  % Combine cells for NN search:
  % superCellGroup = [80, 30, 30];  % how many super-cells to divide into 
  superCell = floor(newDims ./ superCell(1:nDims)); 

  fprintf('Super-cell division: (%s)\n', ...
    strjoin(cellstr(num2str(superCell(:))),','));


  % Iterating over (new) cells:
  fprintf('Iterating over cells ...\n\tProgress: 0%%\n');

  % Progress update frequency 
  everyPercent = 5;   % update progress every (value) percent
  progressSteps = (100 / everyPercent) + 1; % number of steps
  progress = round( linspace(0, newDims(end), progressSteps) ); % boundaries
  p = 1;  % progress counter
  tic   
  timeCurr = [toc]; % record current time

  switch nDims
    case 1
      % 1D reduction:
      for ci = 1:superCell(1):newDims(1)
        % Upper super-cell boundary:
        cim = ci + superCell(1);  % super-cell upper x-boundary

        % Find particles in cell {i}
        part_i = find( (cx >= ci) & (cx < cim) )

        % Create distribution
        if ( smethod == 'rand' )
          % create random permutation of particle IDs
          momID = randperm(part_ij);

        elseif ( smethod == 'p_dist' )
          % calculate total momentum |p^2| = sqrt(sum(p_{i}.^2)), i \in {x,y,z}
          totalMom = [sqrt( fVar.('Px')(part_i).^2 + ... 
                    fVar.('Py')(part_i).^2 + fVar.('Pz')(part_i).^2  )];

          % sort total momenta:
          [totalMom, momID] = sort(totalMom);  
        end

        % Sample from distribution:
        sampleFreq = prod(oldDims ./ newDims);
        samples = round(1:sampleFreq:length(part_i));

        % Sampled particle IDs:
        sampledMomID = momID(samples);            % momentum sampled IDs
        sampledID = part_i(sampledMomID);        % global sampled IDs
        discardedID = setdiff(part_i, sampledID); % global discarded IDs

        % Array of sampled particle IDs
        part_keep( (end+1):(end+length(sampledID)) ) = sampledID; 

        % NN search over discarded for closest kept neighbour; sum weighting 
        % Nearest-neighbour search:
        %{
          k = dsearchn(X, T, XI)
            X : (m,n)-matrix of points
            XI: (p,n)-matrix of p points in n-d space
            T : (nt,n+1)-matrix of triangulation data
            k : output column-vector, length p

          T = delaunayn(X)
            X : (m,n)-matrix of points
            T : (nt,n+1) set of simplices, such that no X are contained in
                circumspheres of the simplices
        %}
        % co-ordinates of sampled particles:
        nnSampled = [fVar.('x')(sampledID)];  
        % co-ordinates of discarded particles:
        nnDiscarded = [fVar.('x')(discardedID)]; 

        % Calculate Delaunay triangulation
        nnTriangulation = delaunayn(nnSampled);
        
        % particle ID of closest particles
        nnClosest = dsearchn(nnSampled, nnTriangulation, nnDiscarded); 

        for ki = 1:length(nnClosest)
          nnClosestSampled = sampledID(nnClosest(ki));
          nnClosestDiscarded = discardedID(nnClosest(ki));
          wSampled = fVar_temp.('w')(nnClosestSampled); % sampled weighting
          wDiscarded = fVar.('w')(nnClosestDiscarded);
          wTotal = wSampled + wDiscarded;
          fVar_temp.('w')(nnClosestSampled) = wTotal;

          if ( skipAverage == 0 )
            for vari = 1:length(tempVarNames)
              % calculate weighted mean of attribute
              vNamei = tempVarNames{vari};
              valSampled = fVar_temp.(vNamei)(nnClosestSampled);
              valDiscarded = fVar.(vNamei)(nnClosestDiscarded);
              fVar_temp.(vNamei)(nnClosestSampled) = (wSampled * ...
                valSampled + wDiscarded * valDiscarded) / wTotal;
            ends
          end
        end

        if ( ci >=  progress(p+1) )
          % update progress 
          timeCurr(end+1) = toc;  % append current time to array
          dt = timeCurr(end) - timeCurr(end-1); % time since last update
          fprintf('\tProgress: %i%% (time: %.1fs, total: %.1fs)\n', ...
            p*everyPercent, dt, timeCurr(end))
          p = p + 1;  % increment progress counter
        end 
      end

    case 2
      % 2D reduction:
      for cj = 1:superCell(2):newDims(2)
        for ci = 1:superCell(1):newDims(1)
          % upper super-cell boundaries:
          cim = ci + superCell(1);  % super-cell upper x-boundary
          cjm = cj + superCell(2);  % super-cell upper y-boundary

          % find particles in cell {i,j}
          part_ij = find( (cx >= ci) & (cx < cim) & (cy >= cj) & (cy < cjm) );

          % Create distribution
          if ( smethod == 'rand' )
            % create random permutation of particle IDs
            momID = randperm(part_ij);
          
          elseif ( smethod == 'p_dist' )
            % calculate total momentum |p^2| = sqrt(sum(p_{i}.^2)), i \in {x,y,z}
            totalMom = [sqrt( fVar.('Px')(part_ij).^2 + ... 
                      fVar.('Py')(part_ij).^2 + fVar.('Pz')(part_ij).^2  )];

            % sort total momenta:
           [totalMom, momID] = sort(totalMom);  
          end
      
          % Sample from distribution:
          sampleFreq = prod(oldDims ./ newDims);
          samples = round(1:sampleFreq:length(part_ij));

          % Sampled particle IDs:
          sampledMomID = momID(samples);            % momentum sampled IDs
          sampledID = part_ij(sampledMomID);        % global sampled IDs
          discardedID = setdiff(part_ij,sampledID); % global discarded IDs

          % Array of sampled particle IDs
          part_keep( (end+1):(end+length(sampledID)) ) = sampledID; 

          % NN search over discarded for closest kept neighbour; sum weighting 
          % Nearest-neighbour search:
          %{
            k = dsearchn(X, T, XI)
              X : (m,n)-matrix of points
              XI: (p,n)-matrix of p points in n-d space
              T : (nt,n+1)-matrix of triangulation data
              k : output column-vector, length p

            T = delaunayn(X)
              X : (m,n)-matrix of points
              T : (nt,n+1) set of simplices, such that no X are contained in
                  circumspheres of the simplices
          %}

          % co-ordinates of sampled particles:
          nnSampled = [fVar.('x')(sampledID) fVar.('y')(sampledID)];  
          % co-ordinates of discarded particles:
          nnDiscarded = [fVar.('x')(discardedID) fVar.('y')(discardedID)]; 

          % Calculate Delaunay triangulation
          nnTriangulation = delaunayn(nnSampled);
          
          % particle ID of closest particles
          nnClosest = dsearchn(nnSampled, nnTriangulation, nnDiscarded); 

          for ki = 1:length(nnClosest)
            nnClosestSampled = sampledID(nnClosest(ki));
            nnClosestDiscarded = discardedID(nnClosest(ki));
            wSampled = fVar_temp.('w')(nnClosestSampled); % sampled weighting
            wDiscarded = fVar.('w')(nnClosestDiscarded);
            wTotal = wSampled + wDiscarded;
            fVar_temp.('w')(nnClosestSampled) = wTotal;

            if ( skipAverage == 0 )
              for vari = 1:length(tempVarNames)
                % calculate weighted mean of attribute
                vNamei = tempVarNames{vari};
                valSampled = fVar_temp.(vNamei)(nnClosestSampled);
                valDiscarded = fVar.(vNamei)(nnClosestDiscarded);
                fVar_temp.(vNamei)(nnClosestSampled) = (wSampled * ...
                  valSampled + wDiscarded * valDiscarded) / wTotal;
              end
            end
          end
        end

        if ( cj >=  progress(p+1) )
          % update progress 
          timeCurr(end+1) = toc;  % append current time to array
          dt = timeCurr(end) - timeCurr(end-1); % time since last update
          fprintf('\tProgress: %i%% (time: %.1fs, total: %.1fs)\n', ...
            p*everyPercent, dt, timeCurr(end))
          p = p + 1;  % increment progress counter
        end
      end

    case 3
      % 3D reduction:
      for ck = 1:superCell(3):newDims(3)      % iterate over z
        for cj = 1:superCell(2):newDims(2)    % iterate over y
          for ci = 1:superCell(1):newDims(1)  % iterate over x
            % upper super-cell boundaries:
            cim = ci + superCell(1);  % super-cell upper x-boundary
            cjm = cj + superCell(2);  % super-cell upper y-boundary
            ckm = ck + superCell(3);  % super-cell upper z-boundary

            % find particles in cell {i,j,k}
            part_ijk = find( (cx >= ci) & (cx < cim) & ...
                        (cy >= cj) & (cy < cjm) & (cz >= ck) & (cz < ckm));

            % Create distribution
            if ( smethod == 'rand' )
              % create random permutation of particle IDs
              momID = randperm(part_ijk);
              
            elseif ( smethod == 'p_dist' )
              % calculate total momentum |p^2| = sqrt(sum(p_{i}.^2)), i \in {x,y,z}
              totalMom = [sqrt( fVar.('Px')(part_ijk).^2 + ... 
                        fVar.('Py')(part_ijk).^2 + fVar.('Pz')(part_ijk).^2  )];

              % sort total momenta:
              [totalMom, momID] = sort(totalMom);  
            end
        
            % Sample from distribution:
            sampleFreq = prod(oldDims ./ newDims);
            samples = round(1:sampleFreq:length(part_ijk));

            % Sampled particle IDs:
            sampledMomID = momID(samples);            % momentum sampled IDs
            sampledID = part_ijk(sampledMomID);        % global sampled IDs
            discardedID = setdiff(part_ijk,sampledID); % global discarded IDs

            % Array of sampled particle IDs
            part_keep( (end+1):(end+length(sampledID)) ) = sampledID; 
            % NN search over discarded for closest kept neighbour; sum weighting
                         
            % Nearest-neighbour search:
            %{
              k = dsearchn(X, T, XI)
                X : (m,n)-matrix of points
                XI: (p,n)-matrix of p points in n-d space
                T : (nt,n+1)-matrix of triangulation data
                k : output column-vector, length p

              T = delaunayn(X)
                X : (m,n)-matrix of points
                T : (nt,n+1) set of simplices, such that no X are contained in
                    circumspheres of the simplices
            %}

            % co-ordinates of sampled particles:
            nnSampled = [fVar.('x')(sampledID) fVar.('y')(sampledID) ...
                          fVar.('z')(sampledID)];  
            % co-ordinates of discarded particles:
            nnDiscarded = [fVar.('x')(discardedID) ...
                            fVar.('y')(discardedID) fVar.('z')(discardedID)]; 

            % whos
            nnTriangulation = delaunayn(nnSampled);
            
            % particle ID of closest particles
            nnClosest = dsearchn(nnSampled,nnTriangulation,nnDiscarded); 

            for ki = 1:length(nnClosest)
              nnClosestSampled = sampledID(nnClosest(ki));
              nnClosestDiscarded = discardedID(nnClosest(ki));
              wSampled = fVar_temp.('w')(nnClosestSampled); % sampled weighting
              wDiscarded = fVar.('w')(nnClosestDiscarded);
              wTotal = wSampled + wDiscarded;
              fVar_temp.('w')(nnClosestSampled) = wTotal;

              if ( skipAverage == 0 )
                for vari = 1:length(tempVarNames)
                  % calculate weighted mean of attribute
                  vNamei = tempVarNames{vari};
                  valSampled = fVar_temp.(vNamei)(nnClosestSampled);
                  valDiscarded = fVar.(vNamei)(nnClosestDiscarded);
                  fVar_temp.(vNamei)(nnClosestSampled) = (wSampled * ...
                    valSampled + wDiscarded * valDiscarded) / wTotal;
                end
              end
            end
          end
        end

        if ( ck >=  progress(p+1) )
        % update progress 
          timeCurr(end+1) = toc;  % append current time to array
          dt = timeCurr(end) - timeCurr(end-1); % time since last update
          fprintf('\tProgress: %i%% (time: %.1fs, total: %.1fs)\n', ...
          p*everyPercent, dt, timeCurr(end))
          p = p + 1;  % increment progress counter
        end
      end
  end


  % Remove discarded particles
  fprintf('Removing discarded particles ...\n')
  sortedIDs = sort(part_keep);

  % Variables reduced:
  reducedTypes = {'pos','mom','wgt'};
  reducedVarID = ismember(varNames, reducedTypes); % (k x 2) logical array
  reducedVars = varNames( find(reducedVarID(:,2)), 1 );

  % Assign reduced {x,y,z}, P{x,y,z} data to struct fVarReduced
  for field = 1:length(reducedVars)
    fieldName = reducedVars{field};

    % check whether data was averaged
    if ( isfield(fVar_temp, fieldName) )
      fVarReduced.(fieldName) = fVar_temp.(fieldName)(sortedIDs);
    else
      fVarReduced.(fieldName) = fVar.(fieldName)(sortedIDs);
    end
  end

end