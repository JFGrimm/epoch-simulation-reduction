function newDeck = genInputDeck(fVar, oldDir, newDir, oldDims, ...
  newDims, reduceDim, nDims);
  % Function to generate a new input deck to continue simulation based on 
  % the original input deck.
  % Arguments: 
  %   -> fVar         : struct containing the required simulation data
  %   -> oldDir       : directory containing original input deck
  %   -> newDir       : output directory 
  %   -> oldDims      : (1 x n) array of original simulation dimensions 
  %   -> newDims      : (1 x n) array of reduced simulation dimensions 
  %   -> reduceDim    : (1 x n) array of dimensions to reduce (0/1 = T/F)
  %   -> nDims        : number of simulation dimensions
  % Returns:
  %   -> newDeck  : new input deck 

  % Read original input.deck
  oldDeckID = fopen( sprintf('%s/input.deck', oldDir), 'r' ); % file handle

  oldDeck = {};   % empty cell array to store original input.deck
  nLines = 0;     % number of lines in input.deck

  while true
    fLine = fgetl(oldDeckID); % read next line

    % check whether at end of file
    if( ischar(fLine) ) 
      nLines = nLines + 1;      % increment line counter  
      oldDeck{nLines} = fLine;  % add line to cell array
    else
      % EOF: break loop
      break
    end
  end

  newDeck = oldDeck;
  
  % Replace input deck parameters:

  % Transverse cells (nx,) ny, nz:
  dimX = {'x','y','z'};
  dimX = dimX(1:nDims);

  for dim = 1:nDims
    if (reduceDim(dim) == 1)
      expr = sprintf('n%s = \d+', dimX{dim});
      repl = sprintf('n%s = %i', dimX{dim}, newDims(2));
      newDeck = regexprep(newDeck, expr, repl, 'freespacing');
    end
  end

  % Time t_end
  %time_pico = 1e12 * fVar.('t');
  %t_end = regexp()
  %expr = sprintf('');
  %repl = sprintf('');
  %newDeck = regexprep(newDeck, expr, repl, 'freespacing');

  %x_min, x_max
  x_min = min(fVar.('gx'));
  x_max = max(fVar.('gx'));

  expr = sprintf('x_min = *')


  %bc_x_min

  % 








end