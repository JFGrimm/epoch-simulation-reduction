function [fVar, fVarNames, simDimensions] = loadSDFVars(f, fVar, ...
  fVarNames, fVarLoc)
  % Loads variables in fVarNames and fVarLoc from the SDF, determining 
  % simulation dimension, which is returned by the function. Variables are 
  % added to the fVar struct, with fields named corresponding to fVarNames.
  % Arguments:
  %   -> f          : (struct) containing SDF data as read by GetDataSDF()
  %   -> fVar       : (struct) containing data required for sim. reduction
  %   -> fVarNames  : (n x 2) cell of variable names, type, to be extracted
  %   -> fVarLoc    : (struct) of strings; field references to variables in
  %                   SDF struct f
  % Returns:
  %   -> fVar          : (struct) with required data to continue simulation.
  %                    Has n fields identical to fVarNames{:,1} in 3D, and 
  %                    fewer in 1D or 2D (n-4, n-2 with default variables)
  %   -> fVarNames     : (n x 2) cell of variable names, type, extracted
  %   -> simDimensions : (int) number of EPOCH sim. dimensions \in {1,2,3}

  simDimensions = 3;  % over-written if unable to load y/z data

  % iterate over variables in fVarNames

  nameIndex = 1;
  while ( nameIndex <= size(fVarNames,1) )
    % additional check for 1D / 2D where fVarNames size will change
    % if ( nameIndex > size(fVarNames,1) )
    %   break
    % end

    [varNamei, varTypei] = fVarNames{nameIndex,:};  % variable name, type

    varLoci = fVarLoc.(varTypei);                   % SDF (sub-)fields

    % append .{x,y,z} for positions, .E{x,y,z} for E-fields etc.:
    if iscell(varLoci)  % skip weights
      % consider grid gx requires .x not .gx -> erase(,'g') (no effect otherwise)
      varLoci = strcat(varLoci{1}, erase(varNamei,'g'),varLoci{2});
    end

    varLoci = strsplit(varLoci,'.');  % split into individual field names

    try
      fVar.(varNamei) = getfield(f,varLoci{:}); % assign data to new struct
    catch
      switch varTypei
      case 'grd'
        if (varNamei == 'gx')
          fprintf('\nFatal error: unable to load x-grid. Aborting...')
          quit;
        elseif (varNamei == 'gy')
          fprintf('\nWarning: unable to load y-grid. Assuming 1D run.\n')
          simDimensions = 1;
          [fVarNames, nskip] = delSDFVars(fVarNames, simDimensions);
          nameIndex = nameIndex - nskip;  % correct skipping gy, gz
        else
          fprintf('\nWarning: unable to load z-grid. Assuming 2D run.\n')
          simDimensions = 2;
          [fVarNames, nskip] = delSDFVars(fVarNames, simDimensions);
          nameIndex = nameIndex - nskip;  % correct skipping gz
        end

      case 'pos' 
          fprintf(['\nFatal error: unable to load %s position data. ' ...
                   'Aborting...\n'],varNamei)
          quit;

      case 'wgt'
        fprintf('\nFatal error: unable to load particle weights. Aborting...')
        quit;

      case 'mom'
        fprintf(['\nWarning: unable to load momentum (%s) data, ' ... 
                'setting to 0.\n'],varNamei)
        fVar.(varNamei) = zeros(length(fVar.x),1);

      case {'elc','mag'}
        fprintf(['\nWarning: unable to load field (%s), ' ...
                'setting to 0.\n'],varNamei)
        fVar.(varNamei) = zeros(length(fVar.x),1);

      case 'tim'
        fprintf('\nWarning: unable to load time (%s).')
      end
    end

    nameIndex = nameIndex + 1;
  end
end