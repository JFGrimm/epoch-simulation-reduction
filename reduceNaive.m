function fVarReduced = reduceNaive(fVar, fVarReduced, cellIDs, ...
  smethod, varNames, oldDims, newDims, reduceDim, nDims);
  % Function to re-distribute excess particles equally across all retained
  % particles in the simulation.
  % Arguments: 
  %   -> fVar         : struct containing the required simulation data
  %   -> fVarReduced  : struct of reduced variables
  %   -> cellIDs      : (1 x n) cell array of particle {x,y,z} references
  %   -> varNames     : (k x 2) cell array of variable names, types
  %   -> oldDims      : (1 x n) array of old simulation dimensions 
  %   -> newDims      : (1 x n) array of new simulation dimensions
  %   -> reduceDim    : (1 x n) array of dimensions to reduce (0/1 = T/F)
  %   -> nDims        : number of simulation dimensions n
  % Returns:
  %   -> fVarReduced  : struct of reduced data 

  



end