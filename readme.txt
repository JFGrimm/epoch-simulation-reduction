Using EPOCH simulation reduction:

This MATLAB program is designed to parse an SDF file of an arbitrary timestep at which it is desired to change the simulation dimensions. The SDF file must include:
    - Simulation grids     : {Grid.x (, Grid.y, Grid.z)}
    - Particle weights     : w
    - Particle positions   : {x (, y, z)}
    - Particle momenta     : {Px, Py, Pz} (initialized to zero if not specified)
    - Electric fields      : {Ex, Ey, Ez} (initialized to zero if not specified)
    - Magnetic fields      : {Bx, By, Bz} (initialized to zero if not specified)
The reduced data is output into the chosen directory as individual .dat files; these can be read as inputs by EPOCH to effectively continue the simulation.

User options:
    - newDimensions = [nx (, ny, nz)]    : specifies the number of new grid-cells in the x (, y, z)-directions. Values set to greater than the original, or less than one, are ignored and default to the original simulation values. If more dimensions are specified than the dimensionality of the original simulation, excess simulations are ignored.
    - sampleMethod = {'random', 'p_dist'}: specifies the particle sampling method to use. 'random' selects pseudo-particles to keep randomly, whilst 'p_dist' sorts pseudo-particles by their total momentum, and samples from this distribution, thereby preserving the original momentum (and hence, energy) distribution.
    - reduceMethod = {'naive', 'nearest'}: specifies the method to re-distribute weightings of pseudo-particles which are not sampled. 'naive' calculates the total weighting removed and scales all sampled particle weightings evenly to ensure a consistant number of real electrons in the simulation. 'nearest' uses a nearest-neighbour algorithm to combine particle weightings with the closest (spatially) retained particle.
    - averageCombined = {'none', 'pos', 'mom'}: specifies whether and how to average pseudo-particle properties when combined using the nearest-neighbour reduction method. If 'none' is specified (or the cell is empty), only the weighting is combined, and the properties of the sampled particle are retained. If 'mom' is specified, momenta of the combined pseudo-particles is averaged; if 'pos' is specified, the positions of the pseudo-particles are averaged. If both 'mom' and 'pos' are specified, both position and momentum are averaged.
    - fieldReduce = {'linear', 'nearest', 'cubic', 'makima', 'spline'}: specifies the interpolation method for the field reduction. 'linear' uses linear interpolation, 'nearest' samples the nearest gridpoint, 'cubic' implements cubic interpolation. 'makima' is a modified Akima Hermite interpolation using piecewise polynomials, and 'spline' uses a cubic spline with not-a-knot end conditions.
    - superCell = [Sx (, Sy, Sz)]: specifies the number of "super-cell" divisions in each dimension. Only used when the 'p_dist' sample method is selected, this divides the total simulation grid into Sx*Sy*Sz subgrids over which the momentum distributions are calculated, and the nearest-neighbour algorithm is run. It is required due to limitations of the nearest-neighbour algorithm, which can only handle ~16 million particles. 
    - dr = 'some/input/directory': the directory containing the SDF file to reduce.
    - dr_out = 'some/output/directory': the directory to which the output .dat files are written.
    - dump = (integer): the number of the SDF dump to use (leading zeros automatically added).
    
Continuing the simulation with the reduced dimension(s) 
Using the original input deck as a basis, the following changes are required in order to continue the simulation:
    1. control:
        - change {nx, ny, nz} to the reduced dimensions as required
        - change final simulation time t_end to (t_end(original) - t(reduction))
        - change size of domain (x_min, x_max) to the values written in the log generated when running the reduction.
        
    2. boundaries:
        - change bc_x_min to simple_outflow
    
    3. window:
        - change window_start_time to 0 (assuming window was moving when the simulation was reduced)
        
    4. laser:
        - delete or comment the entire laser control block
        
    5. particles_from_file:
        - add a particles_from_file block to load particle data:
            begin:particles_from_file
              species = electron
              x_data = "x.dat"
              y_data = "y.dat"

              w_data = "w.dat"

              px_data = "Px.dat"
              py_data = "Py.dat"
              pz_data = "Pz.dat"
            end:particles_from_file
    
    6. fields:
        - add a fields block to load electric and magnetic fields:
            begin:fields
              ex = "Ex.dat"
              ey = "Ey.dat"
              ez = "Ez.dat"

              bx = "Bx.dat"
              by = "By.dat"
              bz = "Bz.dat"
            end:fields

    7. output:
        - change output frequency/times as required (e.g. if using dump_at_times, offset all times by the time at which simulation was reduced)
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    