function [fVarNames, fVarLoc] = requiredSDFVars()
  % Function which defines variables and "type" (fVarNames), to load from 
  % the SDF, as well as their (sub-)field location in the SDF dump (fVarLoc). 
  % All (3) dimensions are listed, and automatically removed if simulation 
  % is in lower dimensions, in function loadSDFVars.m.
  % Returns:
  %   -> fVarNames: (k x 2)-cell array of var, type (grd,pos,...)
  %   -> fVarLoc:   struct with fields corresponding to types
  % Variables to load { var1, type1 ; var2, type2 ; etc. } %

  % CONDENSED FORM:
  %fVarNames = {'gx', 'gy', 'gz', 'x', 'y', 'z', 'w', 'Px', 'Py', 'Pz', ...
  %  'Ex', 'Ey', 'Ez', 'Bx', 'By', 'Bz'; ...
  %  'grd', 'grd', 'grd', 'pos', 'pos', 'pos', 'wgt', 'mom', 'mom', 'mom', ... 
  %  'elc', 'elc', 'elc', 'mag', 'mag', 'mag'}';

  % Variable names, type:
  fVarNames = { 'gx', 'grd'; ...  % x-grid
                'gy', 'grd'; ...  % y-grid
                'gz', 'grd'; ...  % z-grid
                'x', 'pos'; ...   % x-positions
                'y', 'pos'; ...   % y-positions
                'z', 'pos'; ...   % z-positions
                'w', 'wgt'; ...   % weights
                'Px', 'mom'; ...  % momenta in x-direction
                'Py', 'mom'; ...  % momenta in y-direction
                'Pz', 'mom'; ...  % momenta in z-direction
                'Ex', 'elc'; ...  % Electric Field in x-direction
                'Ey', 'elc'; ...  % Electric Field in y-direction
                'Ez', 'elc'; ...  % Electric Field in z-direction
                'Bx', 'mag'; ...  % Magnetic Field in x-direction
                'By', 'mag'; ...  % Magnetic Field in y-direction
                'Bz', 'mag'; ...  % Magnetic Field in z-direction
                't', 'tim'; ...   % time
              };    

  % Variable SDF (sub-)field locations: 
  fVarLoc = struct;
  % formats for variables in SDF: %
  fVarLoc.grd = {'Grid.Grid.',''};                % grid      : gx, gy, gz
  fVarLoc.pos = {'Grid.Particles.electron.',''};  % positions : x (, y, z)
  fVarLoc.mom = {'Particles.','.electron.data'};  % momenta   : Px, Py, Pz
  fVarLoc.wgt = 'Particles.Weight.electron.data'; % weights   : w
  fVarLoc.elc = {'Electric_Field.','.data'};      % E-fields  : Ex, Ey, Ez
  fVarLoc.mag = {'Magnetic_Field.','.data'};      % B-fields  : Bx, By, Bz
  fVarLoc.tim = {'time'}                          % time      : t

end