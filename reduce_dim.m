addpath('/home/vol05/scarf545/MScR/epoch_scripts/');  % add script to matlab path

fprintf('reduce_dim.m \n\n')
%{ 
% Matlab script to reduce specified dimensions in an EPOCH simulations,  
% used to effectively continue the simulation lower dimensions. The script 
% requires an SDF dump as input, containing:
%   - particle positions: x {,y,z}  
%   - particle momenta:   Px, Py, Pz    
%   - particle weights:   w
%   - electric fields:    Ex, Ey, Ez
%   - magnetic fields:    Bx, By, Bz
%   - simulation grid:    Grid.x {, Grid.y, Grid.z}
% Note that momenta, and fields may be omitted (thus initialized as 0).
% 
% Data is output into the chosen directory as .dat files, which can be
% loaded as the initial conditions in an input deck using the particles_from_file
% block, ie.:
%   begin:particles_from_file
%     species = 'electron'
%
%     x_data = "x.dat"
%     y_data = "y.dat"
%
%     w_data = "w.dat"
%
%     px_data = "px.dat"
%     py_data = "py.dat"
%     pz_data = "pz.dat"
%   end:particles_from_file
%
% whilst the fields can be loaded within the fields block, ie.:
%   begin:fields
%     ex = "ex.dat"
%     ey = "ey.dat"
%     ez = "ez.dat"
%
%     bx = "bx.dat"
%     by = "by.dat"
%     bz = "bz.dat"
%   end:fields
%
%}



%%% USER INPUT %%%

% Reduced dimensions %
newDimensions= [8000, 300]; % no. of cells in [x (,y, z)]
                        % note: setting a value <= 0 does not reduce dim.
                        %       Likewise for a value greater than original.

% Specify reduction method and sampling scheme %
sampleMethod = 'p_dist';  % Particle sampling: 
                          %   'random' : sample particles randomly
                          %   'p_dist' : sample evenly from momentum 
                          %              distribution (within super-cell)

reduceMethod = 'nearest'; % Particle reduction:
                          %   'naive'  : redistribute pseudo-particle  
                          %              weightings equally
                          %   'nearest': use nearest-neighbour algorithm to
                          %              find closest particle spatially

averageCombined = {'pos', 'mom'};  % Average combined particle attributes  
                          % (ignored if reduceMethod = 'naive'):
                          %   (empty) : retain all attributes of sampled 
                          %             particle (other than weighting)
                          %   'mom'   : average momenta  
                          %   'pos'   : average positions

fieldReduce = 'linear';   % 'linear' : linear interpolation ,
                          % 'nearest': use value of nearest gridpoint,
                          % 'cubic'  : cubic interpolation,
                          % 'makima' : modified Akima Hermite interpolation
                          %            using piecewise polynomials,
                          % 'spline' : cubic spline using not-a-knot end
                          %            conditions
                          % 

% Cell grouping for Nearest Neighbour algorithm:
superCell = [80, 30, 30];  % how many super-cells to divide into (dx,dy,dz)


% Files and directories %
dr = 'res_1';             % directory containing input .SDF file 
dr_out = 'test_mom_pos'; % output directory to write .dat files to
dump = 3;                 % sdf dump to use (int)
sdf = sprintf('%04i.sdf', dump); % add leading zeros to SDF 

% Print key user parameters to output file %
fprintf('Input directory: %s, SDF dump: %s\n', dr, sdf)
fprintf('Output directory: %s\n', dr_out)
fprintf(['Reducing using:\n\t-> sampling: %s\n\t', ...
                           '-> reduction: %s\n\t', ...
                           '-> field reduction: %s\n'], sampleMethod, ...
                           reduceMethod, fieldReduce)
if ( reduceMethod == 'nearest' & ~isempty(averageCombined) )
    fprintf('\t-> particle averaging: %s\n', strjoin(averageCombined, ', '))
end

% Comma seperated dimensions:
fprintf('New dimensions: (%s)\n', ...
  strjoin( cellstr( num2str( newDimensions(:) ) ), ',' ) ); 

cd(dr)  % change to input directory



%%% LOAD DATA %%%

% Load SDF into memory %
fprintf('Loading .sdf into memory...')
SDF = GetDataSDF(sdf);

%% Particle positions %%
fprintf('done.\n\nLoading data from sdf...')

% Load variables to extract, location to extract from:
[SDFVarNames,SDFVarLoc] = requiredSDFVars();

allVar = struct;
[allVar, SDFVarNames, epochDim] = loadSDFVars(SDF, allVar, ...
                                    SDFVarNames, SDFVarLoc);

% De-allocate raw SDF file from memory
clear SDF;


%%% SIMULATION DIMENSIONS %%%
fprintf('\nDetermining simulation dimensions...')

originalDims = zeros(1,epochDim);  % number of original simulation cells
for dim = 1:epochDim
  originalDims(dim) = length( allVar.(SDFVarNames{dim,1}) ) - 1; 
end


%npart = length(allVar.w);   % number of particles 


%%% Reduce Dimension(s) %%%

% Ensure same number of new dimensions and original simulation dimensions 
newDim = length(newDimensions); % number of dimensions in reduced simulation
if ( newDim ~= epochDim )
  fprintf(['\nWarning: dimensionality of original (%iD) and reduced ' ...
    '(%iD) simulations do not match. '], epochDim, newDim)
  dimFix = abs(epochDim - newDim); % number of missing/excess dimensions

  if ( newDim < epochDim )
    % Assume missing dimensions are not reduced, append to newDimensions
    fprintf('Assuming %i missing dimensions are not reduced.\n', dimFix)
    newDimensions( (end+1):(end+dimFix) ) = originalDims( (1+epochDim-...
      dimFix):(end) );
  else
    % Ignore excess dimensions defined, remove from newDimensions
    fprintf('Ignoring %i excess dimensions.\n', dimFix)
    newDimensions( (end+1-dimFix):end ) = [];
  end
end


%% Dimensions to reduce %% 
reducedDimensions = zeros(1,epochDim);  % array of dimensions to reduce:
                                        % [dx,dy,dz]; d{d,y,z} = 0 or 1 (T/F) 
n_reduced = 0;  % no. of dimensions to reduce

for d = 1:epochDim
  % check whether dimension should be reduced
  if( ( newDimensions(d) < originalDims(d) ) && ( newDimensions(d) > 0 ) )
    % 
    reducedDimensions(d) = 1;   % dimension reduced == true
    n_reduced = n_reduced + 1;  % increment count of number of reduced dims
  else 
    % if newDimensions <= 0 or newDimensions >= oldDimensions
    newDimensions(d) = originalDims(d);
  end
end

if (n_reduced == 0)
  % no dimensions chosen to be reduced
  fprintf('\nError: no dimensions selected to reduce. Aborting...\n')  
  quit
end

fprintf('Simulation time: %.16e\n', allVar.('t'))
fprintf('Window x-boundaries: \n\tx_min = %.16f\n\tx_max = %.16f\n', ...
  min(allVar.('gx')), max(allVar.('gx')))


%% Reduce particles and grid %%

% Sample particles
allVarReduced = sampleParticles(allVar, sampleMethod, reduceMethod, ...
  averageCombined, superCell, SDFVarNames, originalDims, newDimensions, ...
  reducedDimensions);

% Reduce fields
allVarReduced = interpolateFields(allVar, allVarReduced, fieldReduce, ...
  epochDim, newDimensions);


%%% Write variables as raw binary files %%%
cd( sprintf('../%s',dr_out) ) % cd to output directory

fprintf('Saving reduced data ...')
for i = 1:length(SDFVarNames)
  varName = SDFVarNames{i};
  datName = sprintf('%s.dat', varName);
  datID = fopen(datName,'w');
  fwrite(datID, allVarReduced.(varName),'double');
  fclose(datID);
end

cd('..')
fprintf(' done!')
