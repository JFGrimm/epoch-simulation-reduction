function fVarReduced = sampleParticles(fVar, smethod, rmethod, ...
  avMethod, superCell, varNames, oldDims, newDims, reduceDim)
  % Function which reduces particles in the simulation by sampling using 
  % the specified method. 
  % Arguments: 
  %   -> fVar      : struct containing the required simulation data
  %   -> smethod   : sample method (currently {'random','p_dist'}), where:
  %                   - 'random' : randomly select particles to keep
  %                   - 'p_dist' : sort particles by magnitude of total 
  %                                momentum; sample from this distribution
  %   -> rmethod   : reduction method (currently {'naive', 'nearest'}):
  %                   - 'naive#  : multiplies chosen particle weightings by  
  %                                a constant factor
  %                   - 'nearest': uses a nearest-neighbour algorithm to  
  %                                add weightings of discarded particles to 
  %                                the closest (spatially) sampled particle
  %   -> avMethod  : (1 x n) cell string array of attributes to average 
  %                  when combining particles using nearest-neighbour method
  %                   - (empty) : retain attributes of sampled particles;  
  %                               only modify particle weighting function
  %                   - 'mom'   : average particle momenta 
  %                   - 'pos'   : average particle positions
  %   -> superCell : (1 x n) array of cell grouping divisions (how many
  %                 supercells to form in each dimension)
  %   -> varNames  : (k x 2) cell array of variable names, types
  %   -> oldDims   : original dimensions of EPOCH simulation (nx {,ny, nz})
  %   -> newDims   : new dimensions (nx {, ny, nz})
  %   -> reduceDim : (1 x n) array of dimensions to reduce (0/1 = T/F)
  % Returns:
  %   -> fVarReduced  : struct of reduced variables


  nDims = length(oldDims);  % number of dimensions
  fVarReduced = struct;

  %% Bin Particles %%

  % Calculate bin edges for new cells %

  for dim = 1:nDims
    % iterate over dimensions {x,y,z}
    gridName = varNames{dim,1};   % = gx, gy, gz
    gridi = fVar.(gridName);      % extract grid data from struct
    fVarReduced.(gridName) = gridi; % copy to reduced-data struct
    if ( reduceDim(dim) == 1 )
      % Create new grid edges if dimension is being reduced % 
      gridMin = min(gridi);   % lower grid boundary
      gridMax = max(gridi);   % upper grid boundary
      fVarReduced.(gridName) = linspace(gridMin, gridMax, ...
                                newDims(dim) + 1)';
    end
    % fix lower boundaries (MATLAB doesn't bin x(i) == xedge_min)
    gridLower = fVarReduced.(gridName)(1);
    fVarReduced.(gridName)(1) = gridLower * ( 1 - 1e-3 * sign(gridLower) );
    
  end


  % Bin pseudoparticles %
  cellIDs = cell(1,3);  % create empty (1x3) cell array of particle cell
                        % references

  
  switch nDims
    case 1
      % 1D binning %
      [N, ~, cx] = histcounts( fVar.('x'), fVarReduced.('gx') );
      % N   : number of pseudo-particles binned into grid cell
      % cx  : cell x-ID (1, 2, ... , nx_new)
      cellIDs{1} = cx;

    case 2
      % 2D binning %
      [N, ~, ~, cx, cy] = histcounts2( fVar.('x'), fVar.('y'), ...
                            fVarReduced.('gx'), fVarReduced.('gy') );
      % N   : number of pseudo-particles binned into grid cell
      % cx  : cell x-ID (1, 2, ... , nx_new)
      % cy  : cell y-ID (1, 2, ... , ny_new)
      cellIDs(1:2) = {cx, cy};

    case 3
      % No native 3D binning algorithm in MATLAB
      % Since N not needed (only need bin positions for each particle), 
      % calculate 1D bins in each dimension {x,y,z}.

      [Nx, ~, cx] = histcounts( fVar.('x'), fVarReduced.('gx') );
      [Ny, ~, cy] = histcounts( fVar.('y'), fVarReduced.('gy') );
      [Nz, ~, cz] = histcounts( fVar.('z'), fVarReduced.('gz') );
      % Nx, Ny, Nz : number of binned particles in each dimensions
      %   ( sum(Nx) = sum(Ny) = sum(Nz) = no. of particles )
      % cx : cell x-ID (1, 2, ... , nx_new)
      % cy : cell y-ID (1, 2, ... , ny_new)
      % cz : cell z-ID (1, 2, ... , nz_new)
      cellIDs = {cx, cy, cz};
      
  end

  switch rmethod
    case 'naive'
      fVarReduced = reduceNaive(fVar, fVarReduced, cellIDs, smethod, ...
        varNames, oldDims, newDims, reduceDim, nDims);

    case 'nearest'
      fVarReduced = reduceNearest(fVar, fVarReduced, cellIDs, smethod, ...
        avMethod, superCell, varNames, oldDims, newDims, reduceDim, nDims);

    otherwise
      fprintf(['Error: invalid reduction method specified; defaulting ' ...
               'to the naive method.\n'])
      fVarReduced = reduceNaive(fVar, fVarReduced, cellIDs, smethod, ...
        varNames, oldDims, newDims, reduceDim, nDims);
  end

  % Restore lower grid boundaries 
  for dim = 1:nDims
    % iterate over dimensions {x,y,z}
    gridName = varNames{dim,1};   % = gx, gy, gz
    fVarReduced.(gridName)(1) = fVar.(gridName)(1);
  end

end