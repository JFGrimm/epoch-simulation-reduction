function fVarReduced = interpolateFields(fVar, fVarReduced, ...
  interpMethod, nDims, newDims)
  % Function which interpolates the electro-magnetic fields from the EPOCH
  % simulation for the new simulation dimensions.
  % Arguments:
  %   -> fVar         : struct of original data
  %   -> fVarReduced  : struct of reduced data
  %   -> interpMethod : string of method to be used for interpolation. See
  %                     griddedInterpolant method documentation for more 
  %                     detail 
  %   -> nDims        : number of simulation dimensions
  %   -> newDims      : reduced simulation dimensions
  % Returns:
  %   -> fVarReduced  : struct of reduced data including fields


  fprintf('Interpolating fields ...\n')

  % Dimensions to reduce:
  dimNames = {'x','y','z'};
  dimNames = dimNames(1:nDims);

  % Field names:
  fieldNames = {'Ex','Ey','Ez','Bx','By','Bz'};

  % Calculate new grid characteristics
  for dim = 1:nDims
    % grid name (gx, gy, gz)
    gridName = sprintf('g%s',dimNames{dim});

    % grid step-size
    gridStep = fVar.(gridName)(2) - fVar.(gridName)(1);

    % convert to grid-cell centres instead of edges
    gridCentres{dim} = fVar.(gridName)(1:end-1) + 0.5 * gridStep; 

    % grid-cell centres for reduced grid
    newCentres{dim} = linspace( min(gridCentres{dim}), ...
                          max(gridCentres{dim}), newDims(dim))';
  end

  switch nDims
    case 1
      % 1D
      GX = ndgrid(gridCentres{:});  % create 1D grid (original dims)
      GXs = ndgrid(newCentres{:});  % create 1D grid (new dims)

      for field = 1:length(fieldNames)
        % iterating over each field (E{x,y,z}, B{x,y,z})
        currName = fieldNames{field};

        % create grid interpolant for original field dimensions
        gridInterp = griddedInterpolant(GX, fVar.(currName), interpMethod);

        % interpolate new gridpoints, assign to fVarReduced
        fVarReduced.(currName) = gridInterp(GXs);
      end

    case 2
      % 2D
      [GX, GY] = ndgrid(gridCentres{:});  % create 2D grid (original dims)
      [GXs, GYs] = ndgrid(newCentres{:}); % create 2D grid (new dims)

      for field = 1:length(fieldNames)
        % iterating over each field (E{x,y,z}, B{x,y,z})
        currName = fieldNames{field}; 

        % create grid interpolant for original field dimensions
        gridInterp = griddedInterpolant(GX, GY, fVar.(currName), ...
          interpMethod);

        % interpolate new gridpoints, assign to fVarReduced
        fVarReduced.(currName) = gridInterp(GXs, GYs);
      end

    case 3
      % 3D
      [GX, GY, GZ] = ndgrid(gridCentres{:});  % create 3D grid (original dims)
      [GXs, GYs, GZs] = ndgrid(newCentres{:});% create 3D grid (new dims)

      for field = 1:length(fieldNames)
        % iterating over each field (E{x,y,z}, B{x,y,z})
        currName = fieldNames{field};

        % create grid interpolant for original field dimensions
        gridInterp = griddedInterpolant(GX, GY, GZ, fVar.(currName), ...
          interpMethod);

        % interpolate new gridpoints, assign to fVarReduced
        fVarReduced.(currName) = gridInterp(GXs, GYs, GZs);
      end
  end
end